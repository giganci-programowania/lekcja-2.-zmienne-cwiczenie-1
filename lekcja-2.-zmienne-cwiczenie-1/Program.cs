﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lekcja_2._zmienne_cwiczenie_1
{
    class Program
    {
        static void Main(string[] args)
        {

            int x = 22;
            long wynik = 22L;
            float y = -0.42f;
            double fizyka = 0.113;
            decimal moneta;
            moneta = 0.22m;

            Console.WriteLine("int: {0}, long: {1}, float: {2}, double: {3}, decimal: {4}", x, wynik, y, fizyka, moneta);

            Console.ReadKey();
        }
    }
}
